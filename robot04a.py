#! /usr/bin/env python
# -*- coding: utf-8 -*-

import rgkit


class Robot:

    def act(self, game):

        # idź do środka planszy, ruch domyślny
        return ['move', rgkit.toward(self.location, rgkit.CENTER_POINT)]

# jeżeli jesteś w środku, broń się
        if self.location == rgkit.CENTER_POINT:
            return ['guard']

# LUB

# jeżeli jesteś w środku, popełnij samobójstwo
        if self.location == rgkit.CENTER_POINT:
            return ['suicide']
# jeżeli obok są przeciwnicy, atakuj
# wersja z pętlą przeglądającą wszystkie pola zajęte przez roboty
        for poz, robot in game.robots.iteritems():
            if robot.player_id != self.player_id:
                if rgkit.dist(poz, self.location) <= 1:
                    return ['attack', poz]
 # idź do środka planszy
        return ['move', rgkit.toward(self.location, rg.CENTER_POINT)]
