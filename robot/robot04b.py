#! /usr/bin/env python
# -*- coding: utf-8 -*-

import rgkit


class Robot:

    def act(self, game):
        # działanie domyślne:
        ruch = ['move', rgkit.toward(self.location, rg.CENTER_POINT)]

        if self.location == rgkit.CENTER_POINT:
            ruch = ['guard']

        for poz, robot in game.robots.iteritems():
            if robot.player_id != self.player_id:
                if rgkit.dist(poz, self.location) <= 1:
                    ruch = ['attack', poz]

        return ruch